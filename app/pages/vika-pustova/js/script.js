// Обьект со всеми личными данными
var userInfo = {
    ["user-name"]: "Vika Pustova",
    ["user-status"]: "Good morning...",
    ["user-group"]: "KN-15-1",
    ["study-place"]: "Kremenchug National University",
    ["user-city"]: "Alexandria",
    ["user-hobbies"]: ['cycling ',' music','photo','volleyball']
};

//Функция, которая срабатывает при загрузке странице
window.onload = function(){
    // Перебор всех свойств обьекта
    for(var key in userInfo){
        // Если свойство является массивом, тогда обьеденяем все хобби в строку через кому
        if(Array.isArray(userInfo[key])){
            setUserData(key,userInfo[key].join(","));
        }else{
            setUserData(key,userInfo[key]);
        }        
    }
    // Получаем новости
    getNews();
	loadToDoList("todolist");
};

// Установка значения по идентификатору в дом обьект и установка его значенияы
function setUserData(id, data){
    document.getElementById(id).innerHTML = data;
}

function getNews(){
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'files/news.json', false);
    xhr.send();

    if (xhr.status != 200) {
        alert( xhr.status + ': ' + xhr.statusText );
    } else {
        JSONReader(JSON.parse( xhr.responseText )); 
    }
}

function JSONReader(jsonData){    
    // Получаем доступ к контейнеру новостей
    var newsContainer = document.getElementById("user-news");    
    var news = jsonData["responseData"]["entries"];
    // Заголовок
    var header = document.createElement("h3");
    header.innerHTML = "News";
    newsContainer.appendChild(header);

    // Перебор все новостей и их вывод в дом
    for (var i = 0; i < news.length; i++)
        {
            var section = document.createElement('section');
            section.className = "news-item";
            newsContainer.appendChild(section);
            var h3 = document.createElement('h3');
            h3.innerHTML = news[i]['title'];
            section.appendChild(h3);
            
            var p = document.createElement('p');
            p.innerHTML = news[i]['contentSnippet'];
            section.appendChild(p);
            
            var link = document.createElement('a');
            var p2 = document.createElement('p');
            link.setAttribute('href', news[i]['link']);
            link.innerHTML = news[i]['link'];
            p2.appendChild(link);
            p2.style = "overflow: hidden;";
            section.appendChild(p2);
        }
}

function addItemToToDoList(){
	var toDoListText = document.getElementById("todolist-text").value;
	var lastItemId = getLastItemId();
	localStorage.setItem(lastItemId != null ? +(lastItemId)+1 : 1, toDoListText);
	var container = document.getElementById("todolist");
	var li = document.createElement("li");
	li.innerHTML = toDoListText;
	li.id = +(lastItemId)+1;
	li.onclick = removeItemFromToDoList;
	container.appendChild(li);
	document.getElementById("todolist-text").value = "";
}

function removeItemFromToDoList(){
	var element = document.getElementById(this.id);
	element.parentNode.removeChild(element);
	localStorage.removeItem(this.id);
}

function loadToDoList(id){
	var container = document.getElementById(id);
	for(var i = 0; i < localStorage.length; i++){
		var li = document.createElement("li");
		li.innerHTML = localStorage.getItem(localStorage.key(i));
		li.id = localStorage.key(i);
		li.onclick = removeItemFromToDoList;
		container.appendChild(li);
	}
}

function getLastItemId(){
	return localStorage.key(localStorage.length-1);
}