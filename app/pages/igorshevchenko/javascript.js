var arr = ["Igor Shevchenko","08.08.19?8","WEB - 1(балл)"]

function init()
{
var p = document.getElementById("name");
p.appendChild(document.createTextNode(arr[0]));
console.log(p);
var t = document.getElementById("date");
t.appendChild(document.createTextNode(arr[1]));
var b = document.getElementById("bal");
b.appendChild(document.createTextNode(arr[2]));

var films = ["Любимые фильмы:","Зелёный слоник","Фарфоровая ваза","Горбатая гора","Человеческая многоножка","Человек швейцарский нож"]
for (var s in films) {
	b.appendChild(document.createElement("br"));
	b.appendChild(document.createTextNode(films[s]));
} setInfo();
}

function setNews(info) {
    console.log('setNews');
    var list = document.getElementById('newslist');
    for (var i in info.news) {
        //============
        var ol = document.createElement('ol');
        //============
        var childtitle = document.createElement('h2');
        childtitle.innerHTML = info.news[i].title;
        //============
        var childtext = document.createElement('p');
        childtext.innerHTML = info.news[i].contentSnippet;
        //============
        var childlink = document.createElement('a');
        childlink.href = info.news[i].link;
        childlink.innerHTML = info.news[i].link;
        //============
        ol.appendChild(childtitle);
        ol.appendChild(childtext);
        ol.appendChild(childlink);
        ol.appendChild(document.createElement('hr'));
        //============
        ol.className = 'newselement';
        list.appendChild(ol);
    }
}
function setInfo() {
    console.log('setInfo');
    var xhr = new XMLHttpRequest();
    var url = "info.json";
    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var xhrResp = JSON.parse(this.responseText);
        
            setNews(xhrResp)
        }
    };
    xhr.open('GET', url, true);
    xhr.send();
}