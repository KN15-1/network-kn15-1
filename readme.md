# Social School

Site for Hamsters

### Project requirements

1. site with 2 columns:

        a) left column - avatar and avatar
        b) right column - short information about the person
          - name, surname
          - year
          - hobbies
          

### Specification

(tasks with * is not binding)

1. Use HTM5 and CSS3 

        *create adaptive design
        *use less
        *use bootstrap
    
### Instruction
git:
1. git clone https://gitlab.com/KN15-1/network-kn15-1.git
2. Заходим в папку проэкта и там открываем консоль
3. git branch vasyapupkin - создаем свою ветку
4. git checkout vasyapupkin - переходим на свою ветку
5. git add . - доваляем свои изменения 
6. git commit -m "коментируем изменения"
7. git push origin vasyapupkin - заливаем в gitLab (может запросить логин:kn15-1 и пасс:kn151qwerty)


project:
1. install node.js version 5.9.1 or less
2. In console write `npm install`
2. In console write `bower install`
3. In root folder (not "app") write `npm start` for start http server
4. Open in browser link [http://localhost:8000](http://localhost:8000)

