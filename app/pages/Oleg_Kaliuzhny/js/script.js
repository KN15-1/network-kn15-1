var info = {
    birthday : "December 31, 1997",
    status : "Student",
    hobbies : [ "programming", "Tesla coil", "web-tech" ]
}

document.getElementById('birthday').innerHTML = info.birthday;
document.getElementById('status').innerHTML = info.status;
document.getElementById('hobbies').innerHTML = info.hobbies[0];
for (var i = 1; i < info.hobbies.length; i++)
{
    document.getElementById('hobbies').innerHTML += ", " + info.hobbies[i];
}

getJSON();


function getJSON(){
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'news.json', false);
    xhr.send();

    if (xhr.status != 200) {
        alert( xhr.status + ': ' + xhr.statusText );
    } else {
        success(JSON.parse( xhr.responseText )); 
    }
}

function success(data) {
    var news = document.getElementsByClassName('news')[0];
    for (var i = 0; i < data['responseData']['entries'].length; i++)
    {
        var article = document.createElement('article');
        news.appendChild(article);
        var h3 = document.createElement('h3');
        h3.innerHTML = data['responseData']['entries'][i]['title'];
        article.appendChild(h3);

        var p = document.createElement('p');
        p.innerHTML = data['responseData']['entries'][i]['contentSnippet'];
        article.appendChild(p);

        var link = document.createElement('a');
        link.setAttribute('href', data['responseData']['entries'][i]['link']);
        link.innerHTML = data['responseData']['entries'][i]['link'];
        article.appendChild(link);

        var hr = document.createElement('hr');
        article.appendChild(hr);
    }

}

var tasks = [];

var obj = "";
obj = localStorage.getItem('tasks');
tasks = JSON.parse(obj);
display();

function display(){
    if (tasks != null)
        for (key in tasks)
        {
            var li = document.createElement('li');
            li.innerHTML = tasks[key];
            li.setAttribute('onclick', "remove(" + key +")");
            document.getElementById('tasks').appendChild(li);
        }
    else {
        tasks = [];
    }
}

function add() {
    var val = document.getElementById('text').value;
    tasks = tasks.concat(val);
    localStorage.setItem('tasks', JSON.stringify(tasks));
    var li = document.createElement('li');
    li.innerHTML = val;
    li.setAttribute('onclick', "remove(" + tasks.indexOf(val) +")");
    document.getElementsByTagName('ul')[0].appendChild(li);
}

function remove(key) {
    var index = tasks.indexOf(tasks[key]);
    if (index > -1) {
        tasks.splice(index, 1);
    }
    document.getElementById('tasks').innerHTML = "";
    localStorage.setItem('tasks', JSON.stringify(tasks));
    display();
    
}

var wage = document.getElementById("text");
wage.addEventListener("keydown", function (e) {
    if (e.keyCode === 13) {  //checks whether the pressed key is "Enter"
        add();
        var val = document.getElementById('text');
        val.innerText="";
    }
});