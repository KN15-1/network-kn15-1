function init() {
    console.log('startInit');
    eventsStart();
    infoStart();
    newsStart();
    todoStart();
    console.log('endInit');
}


//=====START=EVENTS=====================
function eventsStart() {
    setDisplayEvent('news');
    setDisplayEvent('todo');
    setTodoAddEvent();
    setInputEvent();
}


function setDisplayEvent(el) {
    var head = document.getElementById(el+'header');
    var content = document.getElementById(el+"wrapper");
    head.onclick = function() {
        content.style.display = (content.style.display == 'block') ? 'none' : 'block';
        sessionStorage.setItem(el+'Stage',content.style.display);
    }
}

function setTodoAddEvent() {
    document.getElementById('addtodo').onclick = function() {addTodo();}
}

function setInputEvent() {
    document.getElementById('inputtodo').onkeydown = function (event) {
        if(event.keyCode == 13)
            addTodo();
    }
}
//=====END=EVENTS=====================
//
//=====START=INFO=====================
function infoStart() {
    console.log('setInfo');
    var xhr = new XMLHttpRequest();
    var url = "info.json";
    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var xhrResp = JSON.parse(this.responseText);
            setName(xhrResp);
            setBirthday(xhrResp);
            setHobby(xhrResp);
            setAvatar(xhrResp);
        }
    };
    xhr.open('GET', url, true);
    xhr.send();
}

function setName(info) {
    console.log('setName');
    document.getElementById('name').innerHTML = info.name;
}

function setBirthday(info) {
    console.log('setBirthday');
    document.getElementById('birthday').innerHTML = info.birthday;
}

function setHobby(info) {
    console.log('setHobby');
    var list = document.getElementById('hobbielist');
    for (var h in info.hobbies) {
        var el = document.createElement('ol');
        list.appendChild(el);
        el.innerHTML = info.hobbies[h];
    }
}

function setAvatar(info) {
    console.log('setAvatar');
    document.getElementById('avatarImg').src = info.avatar;
}

//=====END=INFO=====================
//
//=====START=NEWS===================
function newsStart() {
    console.log('setInfo');
    var xhr = new XMLHttpRequest();
    var url = "news.json";
    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var xhrResp = JSON.parse(this.responseText);
            setNews(xhrResp)
        }
    };
    xhr.open('GET', url, true);
    xhr.send();
    loadNewsStage();
}

function loadNewsStage() {
    var stage = sessionStorage.getItem('newsStage');
    document.getElementById("newswrapper").style.display = stage == null ? 'none' : stage;
}

function setNews(info) {
    console.log('setNews');
    var list = document.getElementById('newslist');
    for (var i in info.news) {
        //============
        var ol = document.createElement('ol');
        //============
        var childtitle = document.createElement('h2');
        childtitle.innerHTML = info.news[i].title;
        //============
        var childtext = document.createElement('p');
        childtext.innerHTML = info.news[i].contentSnippet;
        //============
        var childlink = document.createElement('a');
        childlink.href = info.news[i].link;
        childlink.innerHTML = info.news[i].link;
        //============
        ol.appendChild(childtitle);
        ol.appendChild(childtext);
        ol.appendChild(childlink);
        ol.appendChild(document.createElement('hr'));
        //============
        ol.className = 'newselement';
        list.appendChild(ol);
    }
}

//
//=====START=TO=DO===================
function todoStart(){
    loadTodo();
    loadTodoStage();
}

function loadTodoStage() {
    var stage = sessionStorage.getItem('todoStage');
    document.getElementById("todowrapper").style.display = stage == null ? 'none' : stage;
}

function loadTodo() {
    var list = document.getElementById('todolist');
    list.innerHTML = "";
    for(var key in localStorage) {
        var ol = document.createElement('ol');
        var childtext = document.createElement('b');
        childtext.innerHTML = localStorage.getItem(key);
        ol.appendChild(childtext);
        var b = createButton(key);
        ol.appendChild(b);
        ol.className = "todoelement";
        list.insertBefore(ol,list.firstChild);
    }
}

function addTodo() {
    var d = document.getElementById('inputtodo').value;
    if (d=="") return;
    localStorage.setItem(new Date().getTime(),d);
    loadTodo();
}

function createButton(key) {
    var b = document.createElement('b');
    b.onclick = function () {
        localStorage.removeItem(key);
        this.parentElement.parentElement.removeChild(this.parentElement);
    }
    b.innerText = "("+"x"+")";
    b.className = "closebutton";
    return b;
}
//=====END=TO=DO===================
