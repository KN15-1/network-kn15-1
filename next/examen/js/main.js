$(document).ready(function () {

    function getJson() {
        var xhr = new XMLHttpRequest();
        var url = "questions.json";
        xhr.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var obj = JSON.parse(this.responseText);
                pickRandomProperty(obj);
            }
        };
        xhr.open('GET', url, true);
        xhr.send();
    }

    function pickRandomProperty(obj) {

        var list = document.getElementById('list'),
            maxQuest = 5;

        for (var i = 0; i < maxQuest; i++) {
            var el = document.createElement('p');
            list.appendChild(el);
            var count = 0;
            for (var prop in obj) {
                if (Math.random() < 1 / ++count) {
                    el.innerHTML = "Вопрос #" + obj[prop].id + " : "+'<br>'+ obj[prop].question;
                }
            }
        }
    }

    $('#button').on('click', function () {
        getJson();
        $('#button').remove();
    });
});